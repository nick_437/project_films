// package
import _ from 'lodash'
import cn from 'classnames'
// components
import Step from '../../components/Step/Step'
// style
import css from './ContainerSteps.module.scss'

export default function ContainerSteps({ active }) {
  const steps = [
    {
      "text": "Step 1"
    },
    {
      "text": "Step 2"
    },
    {
      "text": "Step 3"
    }
  ]
  return (
    <div className={css.container}>
      {_.map(steps, (item, key) => (
        <Step
          {...item}
          key={key + 1}
          className={cn(css.step, {
            [css.active]: active === key + 1
          })}
        />
      ))}
    </div>
  )
}
