// packages
import { useCallback } from 'react'
import { useQuery } from 'react-query'
import _ from 'lodash'
import { useDispatch, useSelector } from 'react-redux'
// constants
import { DETAIL_ID } from '../../reducers/detailId'

export default function useFilms() {
  const dispatch = useDispatch()
  const id = useSelector(state => state.detailId.detailId)
  // data films
  const { isLoading, error, data } = useQuery('data', () =>
    fetch('https://swapi.dev/api/films/').then(res =>
      res.json()
    )
  )

  // add id film
  const items =
    _.map(data?.results, (item, key) => ({
      ...item,
      id: key + 1
    }))

  // id film
  const handleClickFilm = useCallback(
    (id) => {
      return (
        dispatch({
          type: DETAIL_ID,
          payload: id
        })
      )
    },
    [dispatch]
  )

  return {
    id,
    data: items,
    handleClickFilm
  }
}
