// package
import _ from 'lodash'
import cn from 'classnames'
// components
import Film from '../../components/Film/Film'
// hooks
import useFilms from './useFilms'
// style
import css from './ContainerFilms.module.scss'

export default function ContainerFilms({ className }) {
  const { id, data, handleClickFilm } = useFilms()
  return (
    <div className={cn(css.wrapper, className)}>
      {_.map(data, (item, key) => (
        <Film
          {...item}
          key={key}
          className={cn(css.film, {
            [css._active]: id === item?.id
          })}
          fnClickFilm={handleClickFilm}
        />
      ))}
    </div>
  )
}
