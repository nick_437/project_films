// package
import cn from 'classnames'
import _ from 'lodash'
// hooks
import useFilmDetail from './useFilmDetail'
// style
import css from './ContainerFilmDetail.module.scss'

export default function ContainerFilmDetail({ className }) {
  const {
    isLoading,
    isError,
    error,
    dataFilm,
    title,
    opening_crawl: openingCrawl
  } = useFilmDetail()

  if (isLoading) {
    return <div className={css.loading}>Loading...</div>
  }

  if (isError) {
    return <div className={css.error}>Error: {error?.message}</div>
  }

  if (_.isEmpty(dataFilm)) {
    return <div className={css.empty}>Please select a movie</div>
  }

  return (
    <div className={cn(css.wrapper, className)}>
      {title && (
        <h1>{title}</h1>
      )}
      {openingCrawl && (
        <p>{openingCrawl}</p>
      )}
    </div>
  )
}
