// packages
import { useQuery } from 'react-query'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react'
// constants
import { DATA_FILM } from '../../reducers/dataFilm'

export default function useFilmDetail() {
  const dispatch = useDispatch()
  const id = useSelector(state => state.detailId.detailId)
  const dataFilm = useSelector(state => state.dataFilm.data)

  // data films
  const { isLoading, isError, error, data } = useQuery(`data[${id}]`, () =>
      fetch(`https://swapi.dev/api/films/${id}`).then(res =>
        res.json()
      ), {
        enabled: !!id
      }
    )

  // redux data film
  useEffect(
    () => {
      dispatch({
        type: DATA_FILM,
        payload: { ...data }
      })
    }, [data, dispatch]
  )

  return {
    ...data,
    dataFilm,
    isError,
    isLoading,
    error
  }
}
