export default function useForm({ setCurrentStep, currentStep }) {
  const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

  const onSubmit = async values => {
    await sleep(1000)
    setCurrentStep(currentStep + 1)
  }

  // valid email
  const email = value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)

  return {
    onSubmit,
    email
  }
}
