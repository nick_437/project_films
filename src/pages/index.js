// components
import Button from '../components/Button/Button'
// containers
import Steps from '../containers/ContainerSteps/ContainerSteps'
import Films from '../containers/ContainerFilms/ContainerFilms'
import FilmsDetail from '../containers/ContainerFilmDetail/ContainerFilmDetail'
import ContainerForm from '../containers/ContainerForm/ContainerForm'
// hooks
import useHoom from './hooks/useHoom'
// style
import css from '../../styles/Home.module.scss'

export default function Home() {
  const {
    currentStep,
    setCurrentStep,
    id,
    handleClickReturnStart,
    dataFilm
  } = useHoom()
  return (
    <div className={css.wrapper}>
      <Steps active={currentStep} />
      {currentStep === 1 && (
        <div className={css.container}>
          <Films className={css.films} />
          <FilmsDetail className={css.films_detail} />
        </div>
      )}
      {currentStep === 2 && (
        <div className={css.container}>
          <ContainerForm
            setCurrentStep={setCurrentStep}
            currentStep={currentStep}
          />
        </div>
      )}
      {currentStep === 3 && (
        <div className={css.container}>
          {`Thanks for the review of the movie ${dataFilm?.title}`}
        </div>
      )}
      {id && (
        <div>
          {currentStep > 1 && currentStep < 3 && (
            <Button
              className={css.btn}
              color='primary'
              variant='contained'
              fnClick={() => setCurrentStep(currentStep - 1)}
            >
              Prev step
            </Button>
          )}
          {currentStep === 1 && (
            <Button
              className={css.btn}
              color='primary'
              variant='contained'
              fnClick={() => setCurrentStep(currentStep + 1)}
            >
              Next step
            </Button>
          )}
          {currentStep === 3 && (
            <Button
              className={css.btn}
              color='primary'
              variant='contained'
              fnClick={handleClickReturnStart}
            >
              Return to start
            </Button>
          )}
        </div>
      )}
    </div>
  )
}
