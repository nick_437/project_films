// package
import cn from 'classnames'
// style
import css from './Step.module.scss'

export default function StepComponent({ text, className }) {
  return (
    <div className={cn(css.container, className)}>
      {text && text}
    </div>
  )
}
