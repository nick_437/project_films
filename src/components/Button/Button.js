export default function ButtonComponent({ className, children, fnClick, ...extraProps }) {
  return (
    <button
      {...extraProps}
      className={className}
      onClick={fnClick}
    >
      {children && children}
    </button>
  )
}
