// package
import cn from 'classnames'
// style
import css from './Film.module.scss'

export default function ComponentsFilm({ title, id, className, fnClickFilm }) {
  return (
    <div
      className={cn(css.container, className)}
      onClick={() => fnClickFilm(id)}
    >
      {title && title}
    </div>
  )
}
