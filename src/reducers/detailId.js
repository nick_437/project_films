import cleanSet from 'clean-set'

export const DETAIL_ID = 'DETAIL_ID'

const initialState = {
  detailId: null
}

export default function DetailId (state = initialState, action) {
  switch (action.type) {
    case DETAIL_ID:
      return cleanSet(state, 'detailId', action.payload)
    default:
      return state
  }
}
